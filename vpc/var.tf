variable "vpc_cidr" {
type = string
default = "192.168.0.0/24" 
}

variable "public_cidrs" {
type = string
default = "192.168.0.16/28"
}

variable "private_cidrs"{
 type = list
 default = ["192.168.0.32/28", "192.168.0.48/28"]
 }

#ingress security group

variable "fromPort" {
  type    = number
  default = 0
}

variable "toPort" {
  type    = number
  default = 0

}

variable "protocol1" {
  type    = string
  default = "-1"

}

variable "cidr1" {
  type    = list
  default = ["0.0.0.0/0"]
  
}

#egress security group


variable "from-Port" {
  type    = number
  default = 0
}

variable "to-Port" {
  type    = number
  default = 0
}


variable "protocol2" {
  type    = string
  default = "-1"
}


variable "cidr2" {
  type    = list
  default = ["0.0.0.0/0"]
}



